import numpy as np
import tensorflow as tf

from flask import Flask, request, jsonify, json
from tensorflow import keras

app = Flask(__name__)

@app.route('/test', methods=['POST', 'GET'])
def index():
    model = tf.keras.models.load_model('saved_model/my_model')

    inputValue = request.json

    data = {
        "user": np.array(inputValue["user"]), 
        "item": np.array(inputValue["item"]), 
        "type": np.array(inputValue["type"]),
    }

    prediction = model.predict(data)

    response = prediction.tolist()

    return jsonify(response)

if __name__ == "__main__":
    app.run(debug=True)