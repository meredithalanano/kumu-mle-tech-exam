# MLE-Technical-Exam
This is a wide and deep recommendation API that accepts three inputs: **user**, **item** and **input**, and outputs a float.

File Structure
====
```
root
└──<API Folder>
└──<Model Folder [saved_model/my_model]>
```



VirtualEnv and Dependencies Installation
====
Before running the API, create a virtual environment and install the dependencies in the **requirements.txt** file.
```console
$ virtualenv API
$ source API/bin/activate
(API)$ pip3 install -r path/to/requirements.txt
```



Running the API
====
Run the python file using the virtual environment:
```
(API)$ python3 app.py
```


Documentation
====


Endpoint
----
The API can be accessed through the endpoint */test* which calls a POST request.
```
POST: http://localhost:5000/test
```


Parameters
----
The API accepts three parameters, **user**, **item** and **input** in JSON format, and outputs a float in JSON format.

| Parameter   | Type        |
| ----------- | ----------- |
| user        | string      |
| item        | string      |
| type        | float       |



Sample Request 1:
----
Here is a sample input request in JSON format:
```json
{
    "user": ["adgslva12yvafd"], 
    "item": ["iDAjgfdus9303igh"], 
    "type": [6.5]
}
```


Sample Response 1:
----
```json
[
    [
        9.28228759765625
    ]
]
```


Sample Request 2:
----
```json
{
    "user": ["adgslva12yvafd", "adgslva12yvbfd", "adgslva12yvcfd"], 
    "item": ["iDAjgfdus9303igh", "biPA1S85Edsgl94z", "FJ1HcbdPsu4fHFB3"], 
    "type": [6, 4, 1]
}
```


Sample Response 2:
----
```json
[
    [
        8.59261703491211
    ],
    [
        5.840397357940674
    ],
    [
        1.706478238105774
    ]
]
